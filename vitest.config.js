import { defineConfig } from 'vitest/config'
import vuePlugin from '@vitejs/plugin-vue2'

export default defineConfig({
  plugins: [vuePlugin()],
  test: {
    globals: true,
    environment: 'jsdom',
  },
});