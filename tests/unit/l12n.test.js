import { strings, languageSelector } from '../../js/l12n';
import { mount } from '@vue/test-utils';

// js/l12n.test.js

// Import the strings object from l12n.js
// Assuming l12n.js exports the strings object

describe('Localization Strings', () => {
    // Save the original console methods
    const originalWarn = console.warn;
    const originalError = console.error;
    
    // Mock console methods before all tests
    beforeAll(() => {
        console.warn = vitest.fn();
        console.error = vitest.fn();
    });
    
    // Restore console methods after all tests
    afterAll(() => {
        console.warn = originalWarn;
        console.error = originalError;
    });
    
    // Reset mocks and set default language before each test
    beforeEach(() => {
        vitest.clearAllMocks();
        strings.setLanguage('en');
    });

    it('should have en and fr languages defined', () => {
        expect(strings.data.en).toBeDefined();
        expect(strings.data.fr).toBeDefined();
    });

    it('should get string value for valid key in active language', () => {
        expect(strings.get('title')).toBe('The Rhythm Circle');
        expect(strings.get('play')).toBe('Play');
    });
    
    it('should get nested string value for valid key in active language', () => {
        expect(strings.get('drums')).toEqual(["Snare Drum", "Kick Drum", "Hi-Hat"]);
        expect(strings.get('presets')).toBeInstanceOf(Array);
        expect(strings.get('presets').length).toBeGreaterThan(0);
    });

    it('should switch language when setLanguage is called', () => {
        strings.setLanguage('fr');
        expect(strings.activeLang).toBe('fr');
        expect(strings.get('title')).toBe('Le Cercle Rythmique');
        expect(strings.get('drums')).toEqual(["Caisse claire", "Grosse caisse", "Charleston"]);
    });
    
    it('should default to English for invalid language', () => {
        strings.setLanguage('es'); // Invalid language
        expect(strings.activeLang).toBe('en');
        expect(console.warn).toHaveBeenCalledWith(expect.stringContaining('Invalid language option'));
    });
    
    it('should warn and default to English for non-existent keys', () => {
        const result = strings.get('nonexistentKey');
        expect(console.warn).toHaveBeenCalledWith(expect.stringContaining('No localisation string'));
        expect(result).toBe('<Missing>');
    });
    
    it('should handle nested keys correctly', () => {
        expect(strings.get('drums.0')).toBe('Snare Drum');
        
        strings.setLanguage('fr');
        expect(strings.get('drums.0')).toBe('Caisse claire');
    });
    
    it('should throw error for completely invalid key paths', () => {
        const result = strings.get('invalidKey.invalidSubKey.invalidProperty');
        expect(console.error).toHaveBeenCalledWith(expect.stringContaining('Unknown localisation string'));
        expect(result).toBe('<Missing>');
    });
});
// Tests for the languageSelector component


describe('Language Selector Component', () => {
    it('renders the correct number of language options', () => {
        const wrapper = mount(languageSelector, {
            propsData: {
                languages: ['en', 'fr']
            }
        });
        expect(wrapper.findAll('.languageSwitcher > div')).toHaveLength(2);
    });

    it('displays the correct language text for each option', () => {
        const wrapper = mount(languageSelector, {
            propsData: {
                languages: ['en', 'fr']
            }
        });
        const langDivs = wrapper.findAll('.languageSwitcher > div');
        expect(langDivs.at(0).text()).toBe('en');
        expect(langDivs.at(1).text()).toBe('fr');
    });

    it('emits input event with correct language when clicked', async () => {
        const wrapper = mount(languageSelector, {
            propsData: {
                languages: ['en', 'fr']
            }
        });
        const langDivs = wrapper.findAll('.languageSwitcher > div');
        
        await langDivs.at(0).trigger('click');
        expect(wrapper.emitted().input).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual(['en']);
        
        await langDivs.at(1).trigger('click');
        expect(wrapper.emitted().input[1]).toEqual(['fr']);
    });

    it('validates required props', () => {
        const consoleError = console.error;
        console.error = vitest.fn();
        
        // Should error when languages prop is missing
        mount(languageSelector, {});
        expect(console.error).toHaveBeenCalled();
        
        // Restore console.error
        console.error = consoleError;
    });
    
    it('accepts value prop correctly', () => {
        const wrapper = mount(languageSelector, {
            propsData: {
                languages: ['en', 'fr'],
                value: 'fr'
            }
        });
        expect(wrapper.props().value).toBe('fr');
    });
});