import { shallowMount } from '@vue/test-utils';
import rhythmCircle from '../../js/rhythmCircle.vue'
// Import dependencies for testing Vue components

// Import the component to test

// Mock the rhythmNode component that is used inside rhythmCircle
const mockRhythmNode = {
    props: ['transform', 'isPlaying', 'midiKey', 'innerRadius'],
    template: '<g :transform="transform"></g>'
};

describe('RhythmCircle Component', () => {
    let wrapper;
    
    beforeEach(() => {
        vi.clearAllMocks();
        
        // Mount the component with default props
        wrapper = shallowMount(rhythmCircle, {
            propsData: {
                cx: 100,
                cy: 100,
                id: 'test-circle',
                theta: 0,
            },
            stubs: {
                'rhythm-node': mockRhythmNode
            }
        });
    });

    it('should render with correct props', () => {
        expect(wrapper.props('ndiv')).toBe(8);
        expect(wrapper.props('radius')).toBe(50);
        expect(wrapper.props('cx')).toBe(100);
        expect(wrapper.props('cy')).toBe(100);
        expect(wrapper.props('id')).toBe('test-circle');
        
        // Check if the main circle is rendered correctly
        const mainCircle = wrapper.find('.main-circle');
        expect(mainCircle.exists()).toBe(true);
        expect(mainCircle.attributes('cx')).toBe('100');
        expect(mainCircle.attributes('cy')).toBe('100');
        expect(mainCircle.attributes('r')).toBe('50');
    });

    it('should render correct number of rhythm-nodes', () => {
        // Check default number of nodes (ndiv=8)
        const nodes = wrapper.findAllComponents(mockRhythmNode);
        expect(nodes.length).toBe(8);
        
        // Set a different ndiv
        wrapper.setProps({ ndiv: 16 });
        // Need to wait for the DOM to update
        wrapper.vm.$nextTick(() => {
            const moreNodes = wrapper.findAllComponents(mockRhythmNode);
            expect(moreNodes.length).toBe(16);
        });
        
        // Set another ndiv value
        wrapper.setProps({ ndiv: 4 });
        wrapper.vm.$nextTick(() => {
            const fewerNodes = wrapper.findAllComponents(mockRhythmNode);
            expect(fewerNodes.length).toBe(4);
        });
        
        // Check that nodes receive correct props
        const firstNode = wrapper.findAllComponents(mockRhythmNode).at(0);
        expect(firstNode.props('midiKey')).toBe(39); // Default midiKey
        expect(firstNode.props('isPlaying')).toBeDefined();
        expect(firstNode.props('transform')).toBeDefined();
    });


    it('position method should calculate correct node positions', () => {
        // Test for node at index 0 (top position in the circle)
        const pos0 = wrapper.vm.position(0);
        expect(pos0.x).toBeCloseTo(100); // cx
        expect(pos0.y).toBeCloseTo(50);  // cy - radius

        // Test for node at index 2 (right position in the circle)
        const pos2 = wrapper.vm.position(2);
        expect(pos2.x).toBeCloseTo(150); // cx + radius
        expect(pos2.y).toBeCloseTo(100); // cy
    });

    it('posToTransform method should return correct transform string', () => {
        const position = { x: 120, y: 130 };
        const transform = wrapper.vm.posToTransform(position);
        expect(transform).toBe('translate(120,130)');
    });

    it('genID method should return correct ID string', () => {
        const id = wrapper.vm.genID(3);
        expect(id).toBe('test-circle_inner3');
    });

    it('playingNodes should calculate which node is currently playing', () => {
        // At theta = 0, the first node (index 0) should be playing
        expect(wrapper.vm.playingNodes[0]).toBe(true);
        expect(wrapper.vm.playingNodes.filter(Boolean).length).toBe(1);
        
        // Set theta to 25% of the circle
        wrapper.setProps({ theta: Math.PI / 2 + 0.001 });
        return wrapper.vm.$nextTick().then(() => {
            expect(wrapper.vm.playingNodes[2]).toBe(true);
            expect(wrapper.vm.playingNodes.filter(Boolean).length).toBe(1);
        });
    });

    it('loadRhythm should set node active states correctly', () => {
        // Mock the $refs.nodes array
        wrapper.vm.$refs.nodes = Array(8).fill().map(() => ({ isActive: false }));
        
        // Create a test rhythm pattern
        const rhythm = [true, false, true, false, true, false, true, false];
        
        // Load the rhythm
        wrapper.vm.loadRhythm(rhythm);
        
        // Verify that node active states are set correctly
        for (let i = 0; i < rhythm.length; i++) {
            expect(wrapper.vm.$refs.nodes[i].isActive).toBe(rhythm[i]);
        }
    });

    it('loadRhythm should warn when rhythm length does not match ndiv', () => {
        // Mock console.warn
        console.warn = vi.fn();
        
        // Mock the $refs.nodes array
        wrapper.vm.$refs.nodes = Array(8).fill().map(() => ({ isActive: false }));
        
        // Create a rhythm with invalid length
        const rhythm = [true, false, true];
        
        // Load the rhythm
        wrapper.vm.loadRhythm(rhythm);
        
        // Verify that warning was logged
        expect(console.warn).toHaveBeenCalled();
    });
});