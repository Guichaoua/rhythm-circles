// Import des dépendances nécessaires pour tester un composant Vue
import { mount } from '@vue/test-utils';

// Mock pour synth qui est utilisé dans le composant
window.synth = {
  noteOn: vitest.fn(),
  noteOff: vitest.fn()
};

// Import du composant à tester
// Note: Dans un environnement réel, vous devrez peut-être utiliser un chemin relatif ou configurer vitest
import rhythmNode from '../../js/rhythmNode.vue'

describe('RhythmNode Component', () => {
  let wrapper;
  
  beforeEach(() => {
    // Réinitialiser les mocks avant chaque test
    vitest.clearAllMocks();
    
    // Monter le composant avec des props par défaut
    wrapper = mount(rhythmNode, {
      propsData: {
        innerRadius: 15,
        isPlaying: false,
        midiKey: 39
      }
    });
  });

  it('should render with correct props', () => {
    // Vérifier que le composant a bien reçu les props - SYNTAXE MODIFIÉE
    expect(wrapper.props('innerRadius')).toBe(15);
    expect(wrapper.props('isPlaying')).toBe(false);
    expect(wrapper.props('midiKey')).toBe(39);
    
    // Vérifier que le DOM a bien été construit
    const circle = wrapper.find('circle');
    expect(circle.exists()).toBe(true);
    expect(circle.attributes('r')).toBe('15');
    expect(circle.classes()).not.toContain('active');
    expect(circle.classes()).not.toContain('playing');
  });

  it('should toggle active state when clicked', async () => {
    // Vérifier l'état initial
    expect(wrapper.vm.isActive).toBe(false);
    
    // Simuler un clic sur le cercle
    await wrapper.find('circle').trigger('pointerdown');
    
    // Vérifier que l'état a changé
    expect(wrapper.vm.isActive).toBe(true);
    expect(wrapper.find('circle').classes()).toContain('active');
    
    // Simuler un second clic pour vérifier le toggle
    await wrapper.find('circle').trigger('pointerdown');
    expect(wrapper.vm.isActive).toBe(false);
    expect(wrapper.find('circle').classes()).not.toContain('active');
  });

  it('should play note when isPlaying changes to true and node is active', async () => {
    // Activer le nœud
    await wrapper.find('circle').trigger('pointerdown');
    expect(wrapper.vm.isActive).toBe(true);
    
    // Changer la prop isPlaying
    await wrapper.setProps({ isPlaying: true });
    
    // Vérifier que noteOn a été appelé avec les bons arguments
    expect(window.synth.noteOn).toHaveBeenCalledWith(9, 39, 100);
    expect(window.synth.noteOff).not.toHaveBeenCalled();
  });

  it('should not play note when isPlaying changes but node is inactive', async () => {
    // Le nœud reste inactif
    expect(wrapper.vm.isActive).toBe(false);
    
    // Changer la prop isPlaying
    await wrapper.setProps({ isPlaying: true });
    
    // Vérifier que noteOn n'a pas été appelé
    expect(window.synth.noteOn).not.toHaveBeenCalled();
  });

  it('should apply playing class when isPlaying is true', async () => {
    // Changer la prop isPlaying
    await wrapper.setProps({ isPlaying: true });
    
    // Vérifier que la classe playing est appliquée
    expect(wrapper.find('circle').classes()).toContain('playing');
  });
});