import presets from '../../js/presets.vue';

import { shallowMount } from '@vue/test-utils';


// Mock dependencies
vi.mock('jzz', () => ({
    JZZ:{
        MIDI: {
            SMF: vi.fn().mockImplementation(() => ({
                push: vi.fn(),
                dump: vi.fn().mockReturnValue('midi-data'),
                MTrk: vi.fn().mockImplementation(() => ({
                    smfSeqName: vi.fn().mockReturnThis(),
                    smfBPM: vi.fn().mockReturnThis(),
                    tick: vi.fn().mockReturnThis(),
                    note: vi.fn().mockReturnThis(),
                    smfEndOfTrack: vi.fn().mockReturnThis()
                }))
            }))
        },
        lib: {
            toBase64: vi.fn().mockReturnValue('base64-string')
        }
    }
}));


describe('Presets Component', () => {
    let wrapper;

    // Mock data for testing
    const mockCircle = {
        ndiv: 8,
        $refs: {
            nodes: [
                { isActive: true },
                { isActive: false },
                { isActive: false },
                { isActive: true },
                { isActive: false },
                { isActive: false },
                { isActive: true },
                { isActive: false }
            ]
        },
        $children: [
            { isActive: true },
            { isActive: false },
            { isActive: false },
            { isActive: true },
            { isActive: false },
            { isActive: false },
            { isActive: true },
            { isActive: false }
        ]
    };

    const mockStrings = {
        get: vi.fn().mockImplementation(key => {
            const translations = {
                'drums': ['Snare', 'Kick', 'Hi-Hat'],
                'presets': ['Tresillo', 'Son', 'Shiko', 'Soukous'],
                'exportMidi': 'Export MIDI'
            };
            return translations[key] !== undefined ? translations[key] : key;
        })
    };

    const mockClock = {
        template: '<div></div>',
        data() {
            return {
                subdivisions: [4, 4],
            tempo: 120,
            midiKeys: [38, 36, 42]}
        }    
    };

    beforeEach(() => {
        // Set up the component with mocks
        wrapper = shallowMount(presets, {
            // Mocking the parent component
            parentComponent: mockClock,
            // Mocking the computed properties
            computed: {
                strings: () => mockStrings,
                circles: () => [mockCircle, mockCircle, mockCircle]
            }
        });
    });

    afterEach(() => {
        vi.clearAllMocks();
    });

    it('initializes with default presets', () => {
        const presetsData = wrapper.vm.presets;
        expect(presetsData).toBeDefined();
        expect(presetsData.length).toBeGreaterThan(0);
        expect(presetsData[0].name).toBe('Tresillo');
        expect(presetsData[0].pattern).toEqual([1, 0, 0, 1, 0, 0, 1, 0]);
    });

    it('computes strings from root component', () => {
        expect(wrapper.vm.strings).toBe(mockStrings);
        expect(wrapper.vm.strings.get('presets')).toEqual(['Tresillo', 'Son', 'Shiko', 'Soukous']);
    });

    it('correctly identifies active patterns', () => {
        // First preset should be active since it matches the mock circle pattern
        const activePatterns = wrapper.vm.active;
        expect(activePatterns).toBeInstanceOf(Array);
        expect(activePatterns.length).toBe(3); // 3 circles

        // The first preset (Tresillo) should match our mock circle
        expect(activePatterns[0][0]).toBe(true);

        // Test isActive method directly
        expect(wrapper.vm.isActive([1, 0, 0, 1, 0, 0, 1, 0], mockCircle)).toBe(true);
        expect(wrapper.vm.isActive([1, 1, 0, 1, 0, 0, 1, 0], mockCircle)).toBe(false);
        expect(wrapper.vm.isActive([1, 0, 0, 1], mockCircle)).toBe(false); // Different length
    });

    it('emits rhythm-load event when preset is clicked', async () => {
        const svgElement = wrapper.find('svg');
        await svgElement.trigger('click');

        expect(wrapper.emitted('rhythm-load')).toBeTruthy();
        expect(wrapper.emitted('rhythm-load')[0]).toEqual([0, [1, 0, 0, 1, 0, 0, 1, 0]]); // First track, first preset pattern
    });

    it.skip('exports MIDI when export button is clicked', async () => {
        // Mock document methods
        const originalCreateElement = document.createElement;
        const mockElement = {
            setAttribute: vi.fn(),
            style: {},
            click: vi.fn(),
        };
        document.createElement = vi.fn().mockReturnValue(mockElement);
        document.body.appendChild = vi.fn();
        document.body.removeChild = vi.fn();

        await wrapper.find('.export button').trigger('click');

        expect(document.createElement).toHaveBeenCalledWith('a');
        expect(mockElement.setAttribute).toHaveBeenCalledWith('href', expect.stringContaining('data:audio/midi;base64,'));
        expect(mockElement.setAttribute).toHaveBeenCalledWith('download', 'export.mid');
        expect(mockElement.click).toHaveBeenCalled();
        expect(document.body.appendChild).toHaveBeenCalledWith(mockElement);
        expect(document.body.removeChild).toHaveBeenCalledWith(mockElement);

        // Restore original method
        document.createElement = originalCreateElement;
    });

    it('handles different pattern lengths correctly in isActive', () => {
        const shortPattern = [1, 0, 0, 1];
        expect(wrapper.vm.isActive(shortPattern, mockCircle)).toBe(false);

        const longPattern = [1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0];
        expect(wrapper.vm.isActive(longPattern, mockCircle)).toBe(false);
    });

    it('renders the correct number of rhythms', () => {
        const rows = wrapper.findAll('table tr');
        expect(rows.length).toBe(wrapper.vm.presets.length);
    });
});