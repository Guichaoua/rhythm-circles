import rhythmClock from '../../js/rhythmClock.vue';
import { shallowMount } from '@vue/test-utils';

// Mock dependencies
const mockStrings = {
    get: vitest.fn().mockImplementation(key => {
        const translations = {
            'play': 'Play',
            'stop': 'Stop',
            'tempo': 'Tempo',
            'reset': 'Reset',
            'drums': ['Snare', 'Kick', 'Hi-Hat'],
            'subdivisions': 'Subdivisions',
            'rotation': 'Rotation',
            'presets': ['Tresillo', 'Son', 'Shiko', 'Soukous'],
            'exportMidi': 'Export MIDI'
        };
        return translations[key] !== undefined ? translations[key] : key;
    })
};


// Mock TweenLite
vi.mock('gsap', () => {
    return {
        TweenLite: {
            to: vitest.fn().mockReturnValue({
                pause: vitest.fn()
        })},
        Linear: { easeNone: 'linear-ease-none' }
    }
});

import { TweenLite } from 'gsap';

describe('Rhythm Clock Component', () => {
    let wrapper;
    
    beforeEach(() => {
        // Reset mocks
        vitest.clearAllMocks();
        
        // Create wrapper with mocks
        wrapper = shallowMount(rhythmClock, {
            mocks: {
                strings: mockStrings
            }
        });
        
        // Set up the $refs.circles manually after mounting
        wrapper.vm.$refs = {
            circles: [
            {
                $refs: {
                nodes: [
                    { isActive: false },
                    { isActive: true },
                    { isActive: false },
                    { isActive: true }
                ]
                },
                loadRhythm: vitest.fn()
            },
            {
                $refs: {
                nodes: [
                    { isActive: true },
                    { isActive: false },
                    { isActive: true },
                    { isActive: false }
                ]
                },
                loadRhythm: vitest.fn()
            },
            {
                $refs: {
                nodes: [
                    { isActive: false },
                    { isActive: false },
                    { isActive: true },
                    { isActive: true }
                ]
                },
                loadRhythm: vitest.fn()
            }
            ]
        };
    });

    it('initializes with correct default values', () => {
        expect(wrapper.vm.layerCount).toBe(3);
        expect(wrapper.vm.cycleLength).toBe(2);
        expect(wrapper.vm.subdivisions).toEqual([8, 8, 16]);
        expect(wrapper.vm.tempo).toBe(30);
        expect(wrapper.vm.midiKeys).toEqual([38, 36, 44, 42, 43, 44, 45, 46]);
        expect(wrapper.vm.paused).toBe(true);
        expect(wrapper.vm.t).toBeLessThan(0); // Should be slightly negative
    });

    it('computes theta based on tempo and time', () => {
        wrapper.vm.tempo = 60;
        wrapper.vm.t = 1;
        expect(wrapper.vm.theta).toBeCloseTo(2 * Math.PI, 5);
        
        wrapper.vm.tempo = 30;
        wrapper.vm.t = 1;
        expect(wrapper.vm.theta).toBeCloseTo(Math.PI, 5);
    });

    it('computes hand transform based on theta', () => {
        wrapper.vm.t = 1;
        wrapper.vm.tempo = 30;
        const expectedRotation = 360;
        expect(wrapper.vm.transformHand).toBe(`rotate(${expectedRotation})`);
    });

    it('animateHand starts the animation', () => {
        wrapper.vm.animateHand();
        expect(TweenLite.to).toHaveBeenCalledWith(
            wrapper.vm,
            1e6,
            expect.objectContaining({
                t: 1e6,
                ease: 'linear-ease-none',
                repeat: -1
            })
        );
        expect(wrapper.vm.paused).toBe(false);
    });

    it('pauseHand pauses the animation', () => {
        wrapper.vm.animation = { pause: vitest.fn() };
        wrapper.vm.pauseHand();
        expect(wrapper.vm.animation.pause).toHaveBeenCalled();
        expect(wrapper.vm.paused).toBe(true);
    });

    it('toggleHand switches between play and pause states', () => {
        // Start with paused state
        wrapper.vm.paused = true;
        wrapper.vm.animateHand = vitest.fn();
        
        wrapper.vm.toggleHand();
        expect(wrapper.vm.animateHand).toHaveBeenCalled();
        
        // Reset mock and test pause state
        vitest.clearAllMocks();
        wrapper.vm.paused = false;
        wrapper.vm.pauseHand = vitest.fn();
        
        wrapper.vm.toggleHand();
        expect(wrapper.vm.pauseHand).toHaveBeenCalled();
    });

    it('stopHand resets the clock', () => {
        wrapper.vm.animation = { pause: vitest.fn() };
        wrapper.vm.t = 10;
        
        wrapper.vm.stopHand();
        
        expect(wrapper.vm.animation.pause).toHaveBeenCalled();
        expect(wrapper.vm.t).toBeLessThan(0); // Should be reset to negative value
    });

    it('loadRhythm updates subdivisions and loads pattern', async () => {
        const mockCircle = { loadRhythm: vitest.fn() };
        wrapper.vm.$refs.circles = [mockCircle];
        
        const rhythm = [1, 0, 1, 0];
        await wrapper.vm.loadRhythm(0, rhythm);
        
        expect(wrapper.vm.subdivisions[0]).toBe(4);
        expect(mockCircle.loadRhythm).toHaveBeenCalledWith(rhythm);
    });

    it('resetAll resets all circles', () => {
        const mockCircles = [
            { loadRhythm: vitest.fn() },
            { loadRhythm: vitest.fn() },
            { loadRhythm: vitest.fn() }
        ];
        wrapper.vm.$refs.circles = mockCircles;
        wrapper.vm.subdivisions = [4, 8, 16];
        
        wrapper.vm.resetAll();
        
        expect(mockCircles[0].loadRhythm).toHaveBeenCalledWith([0, 0, 0, 0]);
        expect(mockCircles[1].loadRhythm).toHaveBeenCalledWith([0, 0, 0, 0, 0, 0, 0, 0]);
        expect(mockCircles[2].loadRhythm).toHaveBeenCalledWith(Array(16).fill(0));
    });

    it('shiftClockwise rotates pattern to the right', () => {
        const nodes = [
            { isActive: true },
            { isActive: false },
            { isActive: true },
            { isActive: false }
        ];
        
        wrapper.vm.$refs.circles = [{ $refs: { nodes } }];
        
        wrapper.vm.shiftClockwise(0);
        
        expect(nodes[0].isActive).toBe(false);
        expect(nodes[1].isActive).toBe(true);
        expect(nodes[2].isActive).toBe(false);
        expect(nodes[3].isActive).toBe(true);
    });

    it('shiftCounterclockwise rotates pattern to the left', () => {
        const nodes = [
            { isActive: true },
            { isActive: false },
            { isActive: true },
            { isActive: false }
        ];
        
        wrapper.vm.$refs.circles = [{ $refs: { nodes } }];
        
        wrapper.vm.shiftCounterclockwise(0);
        
        expect(nodes[0].isActive).toBe(false);
        expect(nodes[1].isActive).toBe(true);
        expect(nodes[2].isActive).toBe(false);
        expect(nodes[3].isActive).toBe(true);
    });

    it('keydown listener toggles animation on spacebar', () => {
        wrapper.vm.toggleHand = vitest.fn();
        
        const event = new KeyboardEvent('keydown', { which: 32 });
        document.dispatchEvent(event);
        
        expect(wrapper.vm.toggleHand).toHaveBeenCalled();
    });

    it('genKey generates unique keys for circles', () => {
        expect(wrapper.vm.genKey(0)).toBe('circle0');
        expect(wrapper.vm.genKey(1)).toBe('circle1');
        expect(wrapper.vm.genKey(2)).toBe('circle2');
    });

    it('pathToDrums returns correct image path', () => {
        expect(wrapper.vm.pathToDrums(0)).toBe('/ressources/drums0.png');
        expect(wrapper.vm.pathToDrums(1)).toBe('/ressources/drums1.png');
        expect(wrapper.vm.pathToDrums(2)).toBe('/ressources/drums2.png');
    });

    it('countPageLoad does not call API in localhost', () => {
        window.XMLHttpRequest = vitest.fn().mockImplementation(() => ({
            open: vitest.fn(),
            send: vitest.fn(),
            responseType: '',
            onload: vitest.fn()
        }));
        
        window.location = { hostname: 'localhost' };
        console.log = vitest.fn();
        
        wrapper.vm.countPageLoad();
        
        expect(console.log).toHaveBeenCalledWith('Local host, skipping page load counter');
    });

    it('component correctly renders buttons and controls', () => {
        expect(wrapper.find('button').text()).toBe('Play');
        expect(wrapper.find('input[type="number"]').exists()).toBe(true);
        expect(wrapper.find('svg.clock').exists()).toBe(true);
    });
});