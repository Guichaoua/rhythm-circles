import { defineConfig } from 'vite';
import  vuePlugin  from '@vitejs/plugin-vue2'

export default defineConfig({ 
  plugins: [vuePlugin()],
  // Add Vite-specific configurations here
  build: {
    outDir: 'dist', // Output directory for build files
  },
});