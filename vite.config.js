import { defineConfig } from 'vite';

export default defineConfig({
  // Add Vite-specific configurations here
  build: {
    outDir: 'dist', // Output directory for build files
  },
});