import globals from "globals";
import pluginJs from "@eslint/js";
import pluginVue from "eslint-plugin-vue";
import pluginVitest from "eslint-plugin-vitest";


/** @type {import('eslint').Linter.Config[]} */
export default [
  {
    files: ["**/*.{js,mjs,cjs,vue}"],
  },
  {
    ignores:["node_modules/*", "dist/*", "build/*", "*.min.js", "coverage/*"]
  },
  {
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.vitest, // Add Vitest globals
      },
    },
  },
  pluginJs.configs.recommended,
  ...pluginVue.configs["flat/essential"],
  pluginVitest.configs.recommended,
];