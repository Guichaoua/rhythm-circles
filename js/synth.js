import {JZZ} from 'jzz'
import { Tiny } from 'jzz-synth-tiny';
Tiny(JZZ)

let useTinySynth = true
var synth
if (useTinySynth){
    synth = JZZ.synth.Tiny()
}


export default synth