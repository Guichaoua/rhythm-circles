
let rhythmNode = {
    props: {
        innerRadius: {
            type: Number,
            default: 10
        },
        isPlaying: {
            type: Boolean
        },
        midiKey:{
            type: Number,
            default:39
        }
    },
    data: function() {
        return {
            isActive: false
        }
    },
    methods: {
        toggle: function(){
            this.isActive = ! this.isActive
        }
    },
    watch: {
        isPlaying(newValue,) {
            if (this.isActive) {
                if (newValue){
                    window.synth.noteOn(9, this.midiKey, 100)
                }else{
                    // window.synth.noteOff(9, this.midiKey)
                }
            }
        }
    },
    template: `
        <circle class="inner-circle" :class="{active:isActive, playing: isPlaying}"
            :r="innerRadius"
            @pointerdown="toggle()"
        />
        `
}

export default rhythmNode;