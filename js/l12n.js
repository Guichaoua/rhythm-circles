// ============================================================================
// i18n Strings

function resolve(path, obj=self, separator='.') {
    var properties = Array.isArray(path) ? path : path.split(separator)
    return properties.reduce((prev, curr) => prev && prev[curr], obj)
}

export const strings = {
    data:{
        en: {
            title: 'The Rhythm Circle',
            drums: ["Snare Drum", "Kick Drum", "Hi-Hat"],
            presets: ['Tresillo', 'Son', 'Shiko', 'Soukous', 'Rumba', 'Bossa Nova', 'Gahu', 'Samba', 
                'Fume-fume',
                'Bembé', 
                'Steve Reich',
                'Basic 1', 'Basic 2', 'Basic 3', 'Basic 4'],
            creditsButton: 'Credits',
            credits: `<h2>CREDITS</h2>
            <p>Developped by Corentin Guichaoua and Paul Lascabettes
            for the project Mathémusique
            <a href="https://www.youtube.com/@mathemusique">www.youtube.com/@mathemusique</a></p>
            <h2>ACKNOWLEDGMENTS</h2>
            <p>Funded by the
            French Ministry of Culture, and supported by the SFAM
            (French Society for Music Analysis) and the AFIM
            (Francophone Association of Music Informatics).
            </p>
            <h2>Contact</h2><a href="mailto:mathemusique.contact@gmail.com">mathemusique.contact@gmail.com</a>
            `,
            reset: 'Reset rhythms',
            play: 'Play',
            stop: 'Pause',
            tempo: 'Tempo (mpm):',
            exportMidi: 'Export',
            rotation: 'Rotation:',
            subdivisions: 'Subdivisions:',
            
        },
        fr: {
            title: 'Le Cercle Rythmique',
            drums: ["Caisse claire", "Grosse caisse", "Charleston"],
            presets: ['Tresillo', 'Son', 'Shiko', 'Soukous', 'Rumba', 'Bossa Nova', 'Gahu', 'Samba', 
                'Fume-fume',
                'Bembé', 
                'Steve Reich',
                'Basique 1', 'Basique 2', 'Basique 3', 'Basique 4',
            ],
            creditsButton: 'Crédits',
            credits: `<h2>CRÉDITS</h2>
            <p>Développé par Corentin Guichaoua et Paul Lascabettes
            pour la chaîne Mathémusique
            <a href="https://www.youtube.com/@mathemusique">www.youtube.com/@mathemusique</a>
            </p>
            <h2>REMERCIEMENTS</h2>
            <p>
            Financé par le ministère
            de la Culture, et soutenu par la SFAM (Société Française d'Analyse Musicale) et l’AFIM (Association Francophone d'Informatique Musicale).
            </p>

            <h2>Contact</h2>
            <a href="mailto:mathemusique.contact@gmail.com">mathemusique.contact@gmail.com</a>
            `,
            reset: 'Réinitialiser',
            play: 'Play',
            stop: 'Pause',
            tempo: 'Tempo (mpm)\xa0:',
            exportMidi: 'Exporter',
            rotation: 'Rotation\xa0:',
            subdivisions: 'Subdivisions :',
        },
    },
    get(key){
        let string = resolve(key,this.data[this.activeLang]);
        if(!string){
            console.warn(`No localisation string for "${key}", defaulting to English`)
            string = resolve(key,this.data.en);
            if(!string){
                console.error(`Unknown localisation string "${key}"`)
                string = "<Missing>"
            }
        }
        return string
    },
    setLanguage(lang){
        if(!this.data[lang]){
            console.log(this.data)
            console.warn(`Invalid language option "${lang}", defaulting to English`);
            this.activeLang='en'
        }else{
            this.activeLang=lang;
        }
    }
}

const search = location.search.match(/hl=(\w*)/);
const language = Object.prototype.hasOwnProperty.call(strings, search) ? search[1] : 'en';

strings.setLanguage(language);


export let languageSelector = {
    props:{
        value:{
            type:String
        },
        languages:{
            type:Array,
            required:true
        }
    },
    template:`
        <div class="languageSwitcher">
            <div v-cloak v-for="lang of languages" 
            @click="$emit('input',lang)">
                {{ lang }}
            </div>
        </div>
    `
}


export default {strings, languageSelector};