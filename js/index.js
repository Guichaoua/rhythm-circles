export var main
import synth from './synth.js'
window.synth = synth
import rhythmClock from './rhythmClock.vue'
import { strings, languageSelector } from './l12n.js'
import creditScreen from './creditScreen.vue'
import Vue from 'vue/dist/vue.js'


main = new Vue({
    el: '#main',
    data: function(){return {
        strings: strings
    }},
    components:{rhythmClock,languageSelector, creditScreen},
    mounted() {
        
    },
})